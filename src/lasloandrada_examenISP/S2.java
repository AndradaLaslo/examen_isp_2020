package lasloandrada_examenISP;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class S2 extends JFrame{



    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JLabel ur;
    JLabel u2;
    JLabel u3;
    JTextField t1;
    JTextField t2;
    JTextField t3;

    JButton bSearch;

    S2(){



        setTitle("Show Content");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(350,250);
        setVisible(true);
    }

    public void init(){





        this.setLayout(null);
        int width=100;int height = 20;

        ur = new JLabel("Primul nr: ");
        ur.setBounds(10, 10, width, height);

        u2 = new JLabel("Al doilea nr ");
        u2.setBounds(10, 40, width, height);

        u3 = new JLabel("Rezultat ");
        u3.setBounds(10, 70, width, height);


        t1 = new JTextField();
        t1.setBounds(75,10,100, height);

        t2 = new JTextField();
        t2.setBounds(75,40,100, height);

        t3 = new JTextField();
        t3.setBounds(75,70,100, height);
        t3.setEditable(false);


        bSearch = new JButton("Calculeaza");
        bSearch.setBounds(75,100,100, height);

        bSearch.addActionListener(new TratareButon());


        add(ur);add(bSearch);
        add(u2);add(u3);add(t1);add(t2);add(t3);

    }

    public static void main(String[] args) {
        new  S2();
    }

    class TratareButon implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String op1 = S2.this.t1.getText();
            String op2 = S2.this.t2.getText();
            int a = Integer.parseInt(op1);
            int b = Integer.parseInt(op2);

            int suma=a+b;

            String str = Integer.toString(suma);

            S2.this.t3.setText(str);





        }
    }
}

